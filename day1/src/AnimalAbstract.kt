abstract class Animal (var noOflegs: Int, var food: String){
    abstract fun animalSound(): String
    open fun getSound(): String {
        return " has $noOflegs legs, eat $food "
    }
}

 class Dog(noOflegs: Int, food: String, var name: String) : Animal(noOflegs,food) {
     override fun animalSound(): String {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun getSound(): String {
        return "Dog"+super.getSound()+"Bok Bok"
    }
}

class Lion(noOflegs: Int, food: String, var name: String) : Animal(noOflegs,food) {
    override fun animalSound(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getSound(): String {
        return "Lion"+super.getSound()+"Arkk Arkk"
    }
}


fun main(args: Array<String>) {
    val Dog: Dog = Dog(4, "milk", "Lucy")
    val Lion: Lion = Lion(4, "meat", "Lucky")
    println(Dog.getSound())
    println(Lion.getSound())
}
