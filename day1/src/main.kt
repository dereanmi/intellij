//Class,Function
//open class Person(var name: String, var surname: String, var gpa: Double) {
//    constructor(name: String,surname: String) : this(name, surname, 0.0){
//
//    }
//    open fun getDetails(): String {
//        return "$name $surname has score $gpa"
//    }
//}
//
//class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
//    override fun getDetails(): String {
//        return super.getDetails() + "and study in $department"
//    }
//}
//
//fun main(args: Array<String>) {
//    val no1: Student = Student("Sonchai", "Pitak", 2.00, "MMIT")
//    val no2: Person = Person("Prayu", "Son")
//    val no3: Person = Person(surname = "Prayu", name = "Son")
//    println(no1.getDetails())
//    println(no2.getDetails())
//    println(no3.getDetails())
//}
//Abstract Method
abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String,surname: String) : this(name, surname, 0.0){

    }
    abstract  fun goodBoy(): Boolean
    open fun getDetails(): String {
        return "$name $surname has score $gpa"
    }
}

class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    override fun goodBoy(): Boolean {
        return gpa > 2.0
    }
}

fun main(args: Array<String>) {
    val no1: Student = Student("Sonchai", "Pitak", 2.00, "MMIT")
    println(no1.getDetails())
}






