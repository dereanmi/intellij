data class Teacher(var name: String, var courseName: String , var shirtColor: Color){

}

fun main(args: Array<String>){
    var teacher1: Teacher = Teacher("Somsak","Demoractic", Color.GREEN)
    var teacher2: Teacher = Teacher("Somsak","Demoractic", Color.BLUE)
    var teacher3: Teacher = Teacher("Somsak","Mathmatics", Color.RED)
    //Show data class of teacher
    println(teacher1)
    println(teacher2)
    println(teacher3)
    //Check all attributes
    println(teacher1.equals(teacher2))
    println(teacher1.equals(teacher3))
    //Show attribute
    println(teacher1.component1())
    println(teacher1.component2())
    //Component
    val (teacherName, teacherCourseName)= teacher3
    println("$teacherName teaches $teacherCourseName")
    //Enumeration
    val (teacherName2, teacherCourseName2, shirtColor)= teacher1
    println("$teacherName2 teaches $teacherCourseName2 and has shirt color is $shirtColor")
    //Odject
    var adHoc = object{
        var x: Int = 0
        var y: Int = 1
    }
    println(adHoc)
    println(adHoc.x + adHoc.y)
    
}

enum class Color{
    RED,GREEN,BLUE
}
