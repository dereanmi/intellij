fun main(args : Array<String>){
    //Function,Defualt function
//   helloWorld()
//    helloMore("Prayuth")
//    helloMore("Sanchai")
//    showName("Thanchanok","Phrompalit")
//    gradeReport("Somchai", 3.33)
//    gradeReport()
//    gradeReport(gpa = 2.50)
//    showNameDefualt(surname = "Phrompalit")
    //Function,if,Call function in new function
//    println(isOdd(5))
//    println(getAbbreviation('A'))
//    println(getGrade(70))
//    println(showGradeStatus(100))
    //For loop
//    for (i in 1..3){
//        println(i)
//    }
//
//    for (i in 6 downTo 0 step 2){
//        println(i)
//    }
    var arrays = arrayOf(5 ,55, 200, 1, 3, 5, 7)
    var max = findMaxValue(arrays)
    println("max value is $max")

}

//Function,Defualt function

fun helloWorld(): Unit {
    println("Hello World")
}

fun helloMore(text: String): Unit {
    println("$text")
}

fun showName(firstname: String, surname: String): Unit {
    println("$surname")
    println("$firstname")
}

fun gradeReport(name: String = "annoymous",
                gpa: Double = 0.00): Unit {
    println("mister $name gpa: is $gpa")

}

fun showNameDefualt(firstname: String = "annoymous",
                    surname: String = "annoymous"): Unit {
    println("$surname")
    println("$firstname")
}

//Function,if,Call function in new function

fun isOdd(value: Int): String {
    if (value.rem (2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr){
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score){
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun showGradeStatus(score: Int): String {
   var grade  = getGrade(score)
    if (grade != null){
        return getAbbreviation(grade.toCharArray()[0])
    }else {
       return "Grade is null"
    }
}

//For loop

fun findMaxValue(arrays: Array<Int>): Int {
    var max: Int = 0
    for(i in arrays.indices){
        if(arrays[i] > max)
        {
            max = arrays[i]
        }
    }
    return max
}





